package aivanz.it.creational.factory;

import aivanz.it.creational.factory.factory.CarFactory;
import aivanz.it.creational.factory.factory.HyundaiCarFactory;
import aivanz.it.creational.factory.product.Car;

/* Creational pattern, it controls class instantiation. 
 * The factory pattern is used to replace class constructors, abstracting the process of object generation 
 * so that the type of the object instantiated can be determined at run-time. */

/* Example:
 * Car factories. 
 * To show a simple example of the factory method design pattern in action, we will create two factories 
 * that generate car objects. Each factory will be responsible for a different manufacturer of car. */

/* FactoryBase:
 * +FactoryMethod(): ProductBase.
 * In this case: CarFactory.
 * This is an abstract base class for the concrete factory classes that will actually create new objects. 
 * This class could be a simple interface containing the signature for the factory method. However, generally 
 * an abstract class will be used so that other standard functionality can be included and inherited by subclasses. 
 * 
 * ConcreteFactory extends FactoryBase:
 * +FactoryMethod(): ProductBase.
 * In this case: HyundaiCarFactory and MazdaCarFactory.
 * The concrete factory class. This class override the "building" method in the abstract CarFactory (FactoryBase) with a 
 * switch to initialize the right object.
 * 
 * ProductBase:
 * In this case: Car.
 * Abstract product class. Nothing to say...
 * 
 * ConcreteProduct extends ProductBase:
 * In this case: HyundaiCoupe, HyundaiI30, MazdaMX5 and Mazda6.
 * The concrete product classes. These are the real products that will be created by HyundaiCarFactory's and MazdaCarFactory's
 * (ConcreteFactory both of them) method CreateCar(String model). */

public class MainClass {
	public static void main(String args[]) {
		CarFactory hyundai = new HyundaiCarFactory();

		try {
			Car coupe = hyundai.CreateCar("coupe");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
