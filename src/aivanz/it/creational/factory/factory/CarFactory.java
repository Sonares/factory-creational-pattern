package aivanz.it.creational.factory.factory;

import aivanz.it.creational.factory.product.Car;

public abstract class CarFactory {
	public abstract Car CreateCar(String model) throws Exception;
}