package aivanz.it.creational.factory.factory;

import aivanz.it.creational.factory.product.Car;
import aivanz.it.creational.factory.product.HyundaiCoupe;
import aivanz.it.creational.factory.product.HyundaiI30;

public class HyundaiCarFactory extends CarFactory {
	@Override
	public Car CreateCar(String model) throws Exception {
		switch (model.toLowerCase()) {
		case "coupe":
			return new HyundaiCoupe();
		case "i30":
			return new HyundaiI30();
		default:
			throw new Exception("Invalid model.");
		}
	}
}
