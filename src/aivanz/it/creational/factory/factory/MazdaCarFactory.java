package aivanz.it.creational.factory.factory;

import aivanz.it.creational.factory.product.Car;
import aivanz.it.creational.factory.product.Mazda6;
import aivanz.it.creational.factory.product.MazdaMX5;

public class MazdaCarFactory extends CarFactory {
	@Override
	public Car CreateCar(String model) throws Exception {
		switch (model.toLowerCase()) {
		case "mx5":
			return new MazdaMX5();
		case "6":
			return new Mazda6();
		default:
			throw new Exception("Invalid model.");
		}
	}
}