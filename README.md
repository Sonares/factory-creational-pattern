#Factory
This is a simple example of Factory, creational design pattern.  

**Description:**  
The factory pattern is used to replace class constructors, abstracting the process of object generation so that the type of the object instantiated can be determined at run-time.  

![PlantUML model] (http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKfCAYufIamkKKZEIImkLd03ClEBAfqIYrCLghcqgs7IjL09tzIIZFmK3KqhXO2Y_9JIv1AGIgvQBYw8TkVylEIYr99Kg6gG4QHfX4egL0EbqDo6h14YZonWKwEhYsuA5zHkJ0LG2IvqQssmg758pKi1XXG0)  
